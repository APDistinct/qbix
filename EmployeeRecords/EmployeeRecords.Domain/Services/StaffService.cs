﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeeRecords.Domain.Models;
using EmployeeRecords.Domain.Repository;

namespace EmployeeRecords.Domain.Services
{
    public class StaffService
    {
        private QbixContext QbixContext { get; set; } = new QbixContext();

        public List<Staff> GetStaffs()
        {
            //var x = QbixContext.Context.GetTable<Skill>().AsQueryable();
            //var y = x.ToList();
            //var t = QbixContext.Context.GetTable<Staff>()/*.ToArray()*/;
            ///*.Select(x => x)*/
            //var v = t/*.AsQueryable()*/.ToList();

            return QbixContext.Context.GetTable<Staff>().ToList();
        }

        public List<StaffSkill> GetStaffSkills(Guid staffId)
        {
            var staffSkills = QbixContext.Context.GetTable<StaffSkill>().Where(a => a.StaffId == staffId).ToList();
            //var skillIds = staffSkills.Select(a => a.SkillId);
            //var skills = QbixContext.Context.GetTable<StaffSkill>().Where(a => skillIds.Contains(a.SkillId)).ToList();
            return staffSkills;
        }

        public List<PersonSkill> GetPersonSkills(Guid personId)
        {
            var personSkills = QbixContext.Context.GetTable<PersonSkill>().Where(a => a.PersonId == personId).ToList();
            //var skillIds = staffSkills.Select(a => a.SkillId);
            //var skills = QbixContext.Context.GetTable<StaffSkill>().Where(a => skillIds.Contains(a.SkillId)).ToList();
            return personSkills;
        }

        public bool Add(Staff staff)
        {
            bool ret = true;
            try
            {
                QbixContext.Context.GetTable<Staff>().InsertOnSubmit(staff);
                QbixContext.Context.SubmitChanges();
            }
            catch
            {
                ret = false;
            }
            return ret;
        }

        public bool Update(Staff staff)
        {
            bool ret = true;
            var table = QbixContext.Context.GetTable<Staff>();
            try
            {
                var t = table.Where(x => x.Id == staff.Id).FirstOrDefault();
                if (t != null)
                {
                    t.Quantity = staff.Quantity;
                    t.Name = staff.Name;
                }
                else
                {
                    table.InsertOnSubmit(staff);
                }
                QbixContext.Context.SubmitChanges();
            }
            catch
            {
                ret = false;
            }
            return ret;
        }

        public bool AddStaffSkill(StaffSkill staffSkill)
        {
            var table = QbixContext.Context.GetTable<StaffSkill>();
            try
            {
                var t = table.Where(x => x.SkillId == staffSkill.SkillId && x.StaffId == staffSkill.StaffId).FirstOrDefault();
                if (t != null)
                {
                    t.Quantity = staffSkill.Quantity;
                }
                else
                {
                    QbixContext.Context.GetTable<StaffSkill>().InsertOnSubmit(staffSkill);
                }
                QbixContext.Context.SubmitChanges();                
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool AddStaffSkillList(List<StaffSkill> staffSkill)
        {
            var table = QbixContext.Context.GetTable<StaffSkill>();
            try
            {

                foreach (var tt in staffSkill)
                {
                    var t = table.FirstOrDefault(x => x.SkillId == tt.SkillId && x.StaffId == tt.StaffId);
                    if (t != null)
                    {
                        t.Quantity = tt.Quantity;
                    }
                    else
                    {
                        QbixContext.Context.GetTable<StaffSkill>().InsertOnSubmit(tt);
                    }
                    QbixContext.Context.SubmitChanges();
                }

            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }

            return true;
        }

        public bool RemoveStaffSkill(List<StaffSkill> staffSkill)
        {
            var table = QbixContext.Context.GetTable<StaffSkill>();
            try
            {

                foreach (var tt in staffSkill)
                {
                    var t = table.FirstOrDefault(x => x.SkillId == tt.SkillId && x.StaffId == tt.StaffId);
                    if (t != null)
                    {
                        QbixContext.Context.GetTable<StaffSkill>().DeleteOnSubmit(tt);
                    }

                    QbixContext.Context.SubmitChanges();
                }

            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }

            return true;
        }

        public bool RemoveStaffSkill(Guid staffId, Guid skillId)
        {
            try
            {
                var staffSkill = QbixContext.Context.GetTable<StaffSkill>().FirstOrDefault(x => x.SkillId == skillId && x.StaffId == staffId);
                if (staffSkill != null)
                {
                    QbixContext.Context.GetTable<StaffSkill>().DeleteOnSubmit(staffSkill);
                    QbixContext.Context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }

            return true;
        }

        public bool AddStaffPerson(List<PersonStaff> staffPersons)
        {
            try
            {
                foreach (var staffPerson in staffPersons)
                {
                    QbixContext.Context.GetTable<PersonStaff>().InsertOnSubmit(staffPerson);
                }

                QbixContext.Context.SubmitChanges();
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        public bool AddPersonSkill(PersonSkill personSkill)
        {
            var table = QbixContext.Context.GetTable<PersonSkill>();
            try
            {
                var t = table.Where(x => x.SkillId == personSkill.SkillId && x.PersonId == personSkill.PersonId).FirstOrDefault();
                if (t != null)
                {
                    t.Quantity = personSkill.Quantity;
                }
                else
                {
                    QbixContext.Context.GetTable<PersonSkill>().InsertOnSubmit(personSkill);
                }
                QbixContext.Context.SubmitChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool AddPersonSkillList(List<PersonSkill> personSkillList)
        {
            var table = QbixContext.Context.GetTable<PersonSkill>();
            try
            {

                foreach (var tt in personSkillList)
                {
                    var t = table.FirstOrDefault(x => x.SkillId == tt.SkillId && x.PersonId == tt.PersonId);
                    if (t != null)
                    {
                        t.Quantity = tt.Quantity;
                    }
                    else
                    {
                        QbixContext.Context.GetTable<PersonSkill>().InsertOnSubmit(tt);
                    }
                    QbixContext.Context.SubmitChanges();
                }

            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }

            return true;
        }

    }
}
