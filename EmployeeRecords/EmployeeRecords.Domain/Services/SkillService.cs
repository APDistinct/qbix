﻿using System.Collections.Generic;
using System.Linq;
using EmployeeRecords.Domain.Models;
using EmployeeRecords.Domain.Repository;

namespace EmployeeRecords.Domain.Services
{
    public interface ISkillService
    {
        List<Skill> GetSkills();
    }

    public class SkillService : ISkillService
    {
        public SkillRepository Repository { get; }

        public SkillService(SkillRepository repository)
        {
            Repository = repository;
        }

        public static SkillService GetSkillService()
        {
            return new SkillService(new SkillRepository());
        }

        public List<Skill> GetSkills()
        {
            return Repository.GetSkills().ToList();
        }

        public void Add(Skill skill)
        {
            Repository.Add(skill);
        }
    }
}
