﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using EmployeeRecords.Domain.Models;
using EmployeeRecords.Domain.Repository;

namespace EmployeeRecords.Domain.Services
{
    public class EmployeeService
    {
        private QbixContext QbixContext { get; set; } = new QbixContext();


        public List<Staff> GetStaffs()
        {
            return QbixContext.Context.GetTable<Staff>().ToList();
        }

        /// <summary>
        /// Вызов хранимой процедуры
        /// </summary>
        /// <returns></returns>
        public List<GetPersonStaffResult> GetPersonStaff()
        {
            return new QQbixContext(QbixContext.ConnectionString).GetPersonStaff().ToList();
        }

        public List<Person> GetPersons()
        {
            return QbixContext.Context.GetTable<Person>().ToList();
        }

        public List<Skill> GetSkills()
        {
            return QbixContext.Context.GetTable<Skill>().ToList();
        }

        public List<Person> GetPersonsByStaff(Guid staffId)
        {
            var staffPersons = QbixContext.Context.GetTable<PersonStaff>().Where(a => a.StaffId == staffId).ToList();
            var personIds = staffPersons.Select(a => a.PersonId);
            var persons = QbixContext.Context.GetTable<Person>().Where(a => personIds.Contains(a.Id)).ToList();

            return persons;
        }

        public List<StaffSkill> GetStaffSkills(Guid staffId)
        {
            return QbixContext.Context.GetTable<StaffSkill>().Where(a => a.StaffId == staffId).ToList();
        }

        public List<PersonSkill> GetPersonSkills(Guid personId)
        {
            return QbixContext.Context.GetTable<PersonSkill>().Where(a => a.PersonId == personId).ToList();

        }
        public List<PersonStaff> GetPersonStaffs(Guid staffId)
        {
            return QbixContext.Context.GetTable<PersonStaff>().Where(a => a.StaffId == staffId).ToList();

        }

        public bool AddPerson(Person person)
        {
            try
            {
                QbixContext.Context.GetTable<Person>().InsertOnSubmit(person);
                QbixContext.Context.SubmitChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool AddStaffPerson(List<PersonStaff> staffPersons)
        {
            try
            {
                foreach (var staffPerson in staffPersons)
                {
                    QbixContext.Context.GetTable<PersonStaff>().InsertOnSubmit(staffPerson);
                }

                QbixContext.Context.SubmitChanges();
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        public bool AddPersonSkill(PersonSkill personSkill)
        {
            var table = QbixContext.Context.GetTable<PersonSkill>();
            try
            {
                var t = table.Where(x => x.SkillId == personSkill.SkillId && x.PersonId == personSkill.PersonId).FirstOrDefault();
                if (t != null)
                {
                    t.Quantity = personSkill.Quantity;
                }
                else
                {
                    QbixContext.Context.GetTable<PersonSkill>().InsertOnSubmit(personSkill);
                }
                QbixContext.Context.SubmitChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool AddPersonSkillList(List<PersonSkill> personSkillList)
        {
            try
            {
                foreach (var tt in personSkillList)
                {
                    var t = QbixContext.Context.GetTable<PersonSkill>()
                        .FirstOrDefault(x => x.SkillId == tt.SkillId && x.PersonId == tt.PersonId);
                    if (t != null)
                    {
                        t.Quantity = tt.Quantity;
                    }
                    else
                    {
                        QbixContext.Context.GetTable<PersonSkill>().InsertOnSubmit(tt);
                    }
                }

                QbixContext.Context.SubmitChanges();

            }

            catch (Exception ex)
            {
                string s = ex.Message;
                QbixContext.Context.Transaction.Rollback();
                return false;
            }


            return true;

        }

        public bool AddStaffSkill(StaffSkill staffSkill)
        {
            var table = QbixContext.Context.GetTable<StaffSkill>();
            try
            {
                var t = table.Where(x => x.SkillId == staffSkill.SkillId && x.StaffId == staffSkill.StaffId).FirstOrDefault();
                if (t != null)
                {
                    t.Quantity = staffSkill.Quantity;
                }
                else
                {
                    QbixContext.Context.GetTable<StaffSkill>().InsertOnSubmit(staffSkill);
                }
                QbixContext.Context.SubmitChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool AddStaffSkillList(List<StaffSkill> staffSkill)
        {
            var table = QbixContext.Context.GetTable<StaffSkill>();
            try
            {

                foreach (var tt in staffSkill)
                {
                    var t = table.FirstOrDefault(x => x.SkillId == tt.SkillId && x.StaffId == tt.StaffId);
                    if (t != null)
                    {
                        t.Quantity = tt.Quantity;
                    }
                    else
                    {
                        QbixContext.Context.GetTable<StaffSkill>().InsertOnSubmit(tt);
                    }
                    QbixContext.Context.SubmitChanges();
                }

            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }

            return true;
        }


        public bool UpdatePerson(Person person)
        {
            try
            {
                var p = QbixContext.Context.GetTable<Person>().FirstOrDefault(x => x.Id == person.Id);
                if (p == null)
                    return false;
                p.NameF = person.NameF;
                p.NameI = person.NameI;
                p.NameO = person.NameO;
                p.Birthday = person.Birthday;
                QbixContext.Context.SubmitChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }


        public bool RemoveStaffSkill(List<StaffSkill> staffSkill)
        {
            var table = QbixContext.Context.GetTable<StaffSkill>();
            try
            {

                foreach (var tt in staffSkill)
                {
                    var t = table.FirstOrDefault(x => x.SkillId == tt.SkillId && x.StaffId == tt.StaffId);
                    if (t != null)
                    {
                        QbixContext.Context.GetTable<StaffSkill>().DeleteOnSubmit(tt);
                    }

                    QbixContext.Context.SubmitChanges();
                }

            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }

            return true;
        }

        public bool RemoveStaffSkill(Guid staffId, Guid skillId)
        {
            try
            {
                var staffSkill = QbixContext.Context.GetTable<StaffSkill>().FirstOrDefault(x => x.SkillId == skillId && x.StaffId == staffId);
                if (staffSkill != null)
                {
                    QbixContext.Context.GetTable<StaffSkill>().DeleteOnSubmit(staffSkill);
                    QbixContext.Context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }

            return true;
        }

        public bool AddPersonStaff(PersonStaff personStaff)
        {
            var table = QbixContext.Context.GetTable<PersonStaff>();
            try
            {
                var t = table.Where(x => x.StaffId == personStaff.StaffId && x.PersonId == personStaff.PersonId).FirstOrDefault();
                if (t != null)
                {
                    t.Quantity = personStaff.Quantity;
                }
                else
                {
                    QbixContext.Context.GetTable<PersonStaff>().InsertOnSubmit(personStaff);
                }
                QbixContext.Context.SubmitChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool AddPersonStaffList(List<PersonStaff> personStaffList)
        {            
            return AddPersonStaffList(personStaffList, out string s);
        }

        public bool AddPersonStaffList(List<PersonStaff> personStaffList, out string errString)
        {
            errString = "";
            var table = QbixContext.Context.GetTable<PersonStaff>();
            try
            {
                foreach (var tt in personStaffList)
                {
                    var t = table.FirstOrDefault(x => x.StaffId == tt.StaffId && x.PersonId == tt.PersonId);
                    if (t != null)
                    {
                        t.Quantity = tt.Quantity;
                    }
                    else
                    {
                        QbixContext.Context.GetTable<PersonStaff>().InsertOnSubmit(tt);
                    }

                }

                QbixContext.Context.SubmitChanges();
            }

            catch (Exception ex)
            {
                errString = ex.Message;
                QbixContext.Context.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, table);
                return false;
            }

            return true;
        }

        public bool DeletePersonSkill(Guid personId, Guid skillId)
        {
            var table = QbixContext.Context.GetTable<PersonSkill>();
            try
            {
                var t = table.FirstOrDefault(x => x.PersonId == personId && x.SkillId == skillId);
                if (t != null)
                {
                    table.DeleteOnSubmit(t);
                    QbixContext.Context.SubmitChanges();
                }
            }

            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }

            return true;
        }

        public bool DeletePersonStaff(Guid personId, Guid staffId)
        {
            var table = QbixContext.Context.GetTable<PersonStaff>();
            try
            {
                var t = table.FirstOrDefault(x => x.PersonId == personId && x.StaffId == staffId);
                if (t != null)
                {
                    table.DeleteOnSubmit(t);
                    QbixContext.Context.SubmitChanges();
                }
            }

            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }

            return true;
        }

        public void EnableTrigger()
        {
            QbixContext.Context.ExecuteCommand("ENABLE TRIGGER [dbo].[PersonStaff_CheckQLimits] ON [dbo].[PersonStaff]");
        }

        public void DisableTrigger()
        {
            QbixContext.Context.ExecuteCommand("DISABLE TRIGGER [dbo].[PersonStaff_CheckQLimits] ON [dbo].[PersonStaff]");
        }
    }
}
