﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeeRecords.Domain.Models;
using EmployeeRecords.Domain.Repository;

namespace EmployeeRecords.Domain.Services
{
    public class PersonService
    {
        private QbixContext QbixContext { get; set; } = new QbixContext();

        public bool Add(Person person)
        {
            try
            {
                QbixContext.Context.GetTable<Person>().InsertOnSubmit(person);
                QbixContext.Context.SubmitChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public List<Person> GetPersons()
        {
            return QbixContext.Context.GetTable<Person>().ToList();
        }

        public List<Person> GetPersonsByStaff(Guid staffId)
        {
            var staffPersons = QbixContext.Context.GetTable<PersonStaff>().Where(a => a.StaffId == staffId).ToList();
            var personIds = staffPersons.Select(a => a.PersonId);
            var persons = QbixContext.Context.GetTable<Person>().Where(a => personIds.Contains(a.Id)).ToList();

            return persons;
        }

        public bool UpdatePerson(Person person)
        {
            try
            {
                var p = QbixContext.Context.GetTable<Person>().Where(x => x.Id == person.Id).FirstOrDefault();
                if (p == null)
                    return false;
                p.NameF = person.NameF;
                p.NameI = person.NameI;
                p.NameO = person.NameO;
                p.Birthday = person.Birthday;
                QbixContext.Context.SubmitChanges();
            }
            catch
            {
                return false;
            }

            return true;
        }

    }
}
