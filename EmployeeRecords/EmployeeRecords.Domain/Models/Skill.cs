﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRecords.Domain.Models
{
    [Table(Name = "Skill")]
    public class Skill
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public Guid Id { get; set; }

        [Column(Name = "Name")]
        public string Name { get; set; }
    }
}
