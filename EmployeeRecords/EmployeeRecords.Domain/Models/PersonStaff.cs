﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRecords.Domain.Models
{
    [Table(Name = "PersonStaff")]
    public class PersonStaff
    {
        [Column(IsPrimaryKey = true)]
        public Guid StaffId { get; set; }
        [Column(IsPrimaryKey = true)]
        public Guid PersonId { get; set; }
        [Column]
        public double Quantity { get; set; }
    }
}
