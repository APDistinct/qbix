﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRecords.Domain.Models
{
    [Table(Name = "PersonSkill")]
    public class PersonSkill
    {
        [Column(IsPrimaryKey = true)]
        public Guid PersonId { get; set; }

        [Column(IsPrimaryKey = true)]
        public Guid SkillId { get; set; }

        [Column]
        public double Quantity { get; set; }
    }
}
