﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRecords.Domain.Models
{
    [Table(Name = "StaffSkill")]
    public class StaffSkill
    {
        [Column(IsPrimaryKey = true)]
        public Guid StaffId { get; set; }
        [Column(IsPrimaryKey = true)]
        public Guid SkillId { get; set; }
        [Column(Name = "Quantity")]
        public double Quantity { get; set; }
    }
}
