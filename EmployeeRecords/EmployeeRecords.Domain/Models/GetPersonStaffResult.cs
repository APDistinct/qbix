﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRecords.Domain.Models
{
    public class GetPersonStaffResult
    {
        [Column]
        public Guid Id { get; set; }
        [Column]
        public string Name { get; set; }
        [Column]
        public double Quantity { get; set; }
        [Column]
        public double QuantityFact { get; set; }
    }
}
