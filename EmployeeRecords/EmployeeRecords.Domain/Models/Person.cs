﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRecords.Domain.Models
{
    [Table(Name = "Person")]
    public class Person
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public Guid Id { get; set; }
        public string Name => $"{NameF} {NameI} {NameO}";

        [Column(Name = "Birthday")]
        public DateTime? Birthday { get; set; }

        [Column(Name = "Foto")]
        public byte[] Foto { get; set; }

        [Column(Name = "NameI")]
        public string NameI { get; set; }
        [Column(Name = "NameO")]
        public string NameO { get; set; }
        [Column(Name = "NameF")]
        public string NameF { get; set; }
    }
}
