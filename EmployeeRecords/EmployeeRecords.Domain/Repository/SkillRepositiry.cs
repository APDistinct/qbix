﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeeRecords.Domain.Models;

namespace EmployeeRecords.Domain.Repository
{
    public class SkillRepository
    {
        private QbixContext QbixContext { get; set; } = new QbixContext();

        public IQueryable<Skill> GetSkills()
        {
            return QbixContext.Context.GetTable<Skill>().AsQueryable();
        }

        public void Add(Skill skill)
        {
            QbixContext.Context.GetTable<Skill>().InsertOnSubmit(skill);
            QbixContext.Context.SubmitChanges();
        }
    }
}
