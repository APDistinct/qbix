﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using EmployeeRecords.Domain.Models;

namespace EmployeeRecords.Domain.Repository
{
    public class QbixContext
    {
        public DataContext Context;

        public string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
            //"Data Source=APD;Initial Catalog=QBIX;User ID=sa;Password=123;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public QbixContext()
        {
            Context = new DataContext(ConnectionString);
        }
    }

    public class QQbixContext : DataContext
    {
        public QQbixContext(string fileOrServerOrConnection) : base(fileOrServerOrConnection)
        {
        }

        /// <summary>
        /// Stored Procedure (dbo.GetPersonStaff)
        /// </summary>
        /// <returns></returns>
        [Function(Name = "dbo.GetPersonStaff")]
        public ISingleResult<GetPersonStaffResult> GetPersonStaff()
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return ((ISingleResult<GetPersonStaffResult>)(result.ReturnValue));
        }
    }    
}
