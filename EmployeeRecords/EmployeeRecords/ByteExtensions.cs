﻿using System.Linq;

namespace EmployeeRecords
{
    public static class ByteExtensions
    {
        static byte[] signatureOffice = { 0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1 };
        static byte[] signatureZip =  { 0x50, 0x4B, 0x03, 0x04 };
        static byte[] signaturePdf = { 0x25, 0x50, 0x44, 0x46 };
        static byte[] signatureJpg = { 0xFF, 0xD8 };
        static byte[] signaturePng = { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A };
        static byte[] signatureTiff = { 0x49, 0x49, 0x2A, 0x00 };
        static byte[] signatureBmp = { 0x42, 0x4D };
        static byte[] signatureGif = { 0x47, 0x49, 0x46, 0x38 };

        public static string GetImageContentType(this byte[] imageData)
        {
            if (imageData.GetFormat() == FileFormat.Bmp) return "image/bmp";
            if (imageData.GetFormat() == FileFormat.Png) return "image/png";
            if (imageData.GetFormat() == FileFormat.Jpg) return "image/jpeg";
            return null;
        }

        public static string GetFileImageType(this byte[] imageData)
        {
            if (imageData.GetFormat() == FileFormat.Bmp) return "image/bmp";
            if (imageData.GetFormat() == FileFormat.Png) return "image/png";
            if (imageData.GetFormat() == FileFormat.Jpg) return "image/jpeg";
            if (imageData.GetFormat() == FileFormat.Gif) return "image/gif";
            if (imageData.GetFormat() == FileFormat.Tiff) return "image/tiff";
            return null;
        }

        public static string GetFileMediaType(this byte[] imageData)
        {
            if (imageData.GetFormat() == FileFormat.Bmp) return "image/bmp";
            if (imageData.GetFormat() == FileFormat.Png) return "image/png";
            if (imageData.GetFormat() == FileFormat.Jpg) return "image/jpeg";
            if (imageData.GetFormat() == FileFormat.Gif) return "image/gif";
            if (imageData.GetFormat() == FileFormat.Tiff) return "image/tiff";
            if (imageData.GetFormat() == FileFormat.Office) return "office";
            if (imageData.GetFormat() == FileFormat.Zip) return "office";
            return null;
        }
        
        public static FileFormat GetFormat(this byte[] file)
        {
            if (signatureOffice.SequenceEqual(file.Take(signatureOffice.Length))) return FileFormat.Office;
            if (signatureZip.SequenceEqual(file.Take(signatureZip.Length))) return FileFormat.Zip;
            if (signaturePdf.SequenceEqual(file.Take(signaturePdf.Length))) return FileFormat.Pdf;
            if (signatureJpg.SequenceEqual(file.Take(signatureJpg.Length))) return FileFormat.Jpg;
            if (signaturePng.SequenceEqual(file.Take(signaturePng.Length))) return FileFormat.Png;
            if (signatureTiff.SequenceEqual(file.Take(signatureTiff.Length))) return FileFormat.Tiff;
            if (signatureBmp.SequenceEqual(file.Take(signatureBmp.Length))) return FileFormat.Bmp;
            if (signatureGif.SequenceEqual(file.Take(signatureGif.Length))) return FileFormat.Gif;
            return FileFormat.Unknown;
        }
    }

    public enum FileFormat
    {
        Unknown,
        Office,
        Zip,
        Pdf,
        Jpg,
        Png,
        Tiff,
        Bmp,
        Gif
    }
}
