﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRecords.Models
{
    public class SkillsViewModel
    {
        public Guid FirstId;
        public Guid SkillId;
        public string SkillName { get; set; }
        public double Quantity { get; set; }
    }
}
