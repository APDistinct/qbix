﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRecords.Models
{
    public class StaffSkillsViewModel
    {
        public Guid StaffId;
        public Guid SkillId;
        public string SkillName { get; set; }
        public double Quantity { get; set; }
    }
}
