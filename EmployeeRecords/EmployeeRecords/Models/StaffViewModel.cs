﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRecords.Models
{
    public class StaffViewModel
    {
        public Guid Id;
        public string Name { get; set; }
        public /*float*/double QuantityShtats { get; set; }
        public /*float*/double QuantityFact { get; set; }
    }
}
