﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRecords.Models
{
    public class PersonModel
    {
        public Guid Id;        
        public string Name { get; set; }
    }
}
