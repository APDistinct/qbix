﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRecords.Models
{
    public class PersonSkillModel
    {
        public Guid PersonId;
        public Guid SkillId;
        public string Name { get; set; }
        //public double QuantitySh { get; set; }
    }
}
