﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeRecords.Domain.Models;
using EmployeeRecords.Domain.Services;
using EmployeeRecords.Models;

namespace EmployeeRecords.Forms
{
    public partial class FormPersons : Form
    {
        //private EmployeeService EmployeeService { get; } = new EmployeeService();
        //private EmployeeService EmployeeService { get; } = EmployeeService.GetEmployeeService();
        private EmployeeService Service { get; } = new EmployeeService();

        private List<Skill> _skills;
        private List<Skill> Skills => _skills ?? (_skills = Service.GetSkills());

        private BindingList<PersonModel> Data { get; set; }
        private List<Person> persons { get; set; }
        //private List<Skill> skills { get; set; }
        private List<PersonSkill> personSkill { get; set; }

        private bool SkillsEditMode { get; set; }
        //private BindingList<StaffViewModel> Data { get; set; } = new BindingList<StaffViewModel>();

        public FormPersons()
        {
            InitializeComponent();
            UpdatePersons();
            InitGrid();
            //SkillsEditMode = false;
            SkillsEditModeChange(false);
        }

        private void SkillsEditModeChange(bool val)
        {
            SkillsEditMode = val;
            buttonAdd.Enabled = !SkillsEditMode;
            buttonEdit.Enabled = !SkillsEditMode;

            buttonEditSkill.Enabled = !SkillsEditMode;
            buttonSave.Enabled = SkillsEditMode;
            buttonCancel.Enabled = SkillsEditMode;            
            buttonDelete.Enabled = !SkillsEditMode;
            buttonAddSkill.Enabled = !SkillsEditMode;
            if (dataGridViewSkills.ColumnCount > 0)
                dataGridViewSkills.Columns[1].ReadOnly = !SkillsEditMode;
            //dataGridViewSkills. .AllowEdit = !SkillsEditMode;
        }


        private void InitGrid()
        {
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            if (dataGridView1.ColumnCount > 0)
            {

                dataGridView1.Columns[0].HeaderText = "ФИО";
                dataGridView1.Columns[0].Width = 200;
                dataGridView1.Columns[0].ReadOnly = true;
            }
            //dataGridView1.Columns[1].HeaderText = "Ставка";
            //dataGridView1.Columns[1].ReadOnly = false;

            if (dataGridView1.RowCount > 0)
                dataGridView1.CurrentCell = dataGridView1[0, 0];
            dataGridView1.Focus();
        }

        private void InitGridViewSkills()
        {
            if (dataGridViewSkills.ColumnCount > 0)
            {
                dataGridViewSkills.Columns[0].HeaderText = "Навыки";
                dataGridViewSkills.Columns[0].ReadOnly = true;
                dataGridViewSkills.Columns[1].HeaderText = "Уровень";
                dataGridViewSkills.Columns[1].ReadOnly = false;
            }

            //dataGridView1.Columns[0].Width = dataGridView1.Width - 50; //400;
        }

        private BindingList<PersonModel> GetPersonList()
        {
            var list = new BindingList<PersonModel>(persons.Select(a => new PersonModel
            {
                Id = a.Id,
                Name = a.Name,
            }).ToList());
            return list;
        }

        private void UpdatePersons()
        {
            persons = Service.GetPersons();
            Data = new BindingList<PersonModel>(GetPersonList())
            { AllowNew = false, AllowEdit = true, AllowRemove = false };
            dataGridView1.DataSource = Data;

            if (dataGridView1.RowCount > 0)
                dataGridView1.CurrentCell = dataGridView1[0, 0];
            dataGridView1.Focus();
        }

        private void InsNew()
        {
            var form = new FormPersonAdd();
            if (form.ShowDialog() == DialogResult.OK)
            {
                if (Service.AddPerson(form.Person))
                    UpdatePersons();
            }
        }

        private IList<SkillsViewModel> GetSkillsViewModel(List<Skill> skills)
        {
            return skills.Select(a => new SkillsViewModel
            {
                SkillId = a.Id,
                SkillName = a.Name,

            }).ToList();
        }

        private List<SkillsViewModel> GetSkillsViewModel(List<PersonSkill> skills, Guid firstId)
        {
            return skills.Select(a => new SkillsViewModel
            {
                FirstId = firstId,
                SkillId = a.SkillId,
                Quantity = a.Quantity,
                SkillName = Skills.First(b => b.Id == a.SkillId).Name
            }).ToList();
        }

        private void ShowSkills(DataGridViewCellEventArgs e)
        {
            var rowindex = e.RowIndex;

            if (rowindex < 0)
                return;
            UpdateSkills(rowindex);
        }

        private void UpdateSkills()
        {
            if (dataGridView1.CurrentRow == null)
                return;
            var index = dataGridView1.CurrentRow.Index;
            UpdateSkills(index);
        }

        private void UpdateSkills(int index)
        {
            if (Data == null || index > Data.Count)
                return;

            var person = Data[index];
            var skills = Service.GetPersonSkills(person.Id);

            var data = new BindingList<SkillsViewModel>(GetSkillsViewModel(skills, person.Id))
                {AllowNew = false, /*AllowEdit = true,*/ AllowRemove = false};
            data.AllowEdit = true;
            dataGridViewSkills.DataSource = data;
            InitGridViewSkills();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            InsNew();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Insert)
            {
                InsNew();
            }
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            ShowSkills(e);
        }

        private void buttonAddSkill_Click(object sender, EventArgs e)
        {
            InsNewSkill();
        }

        private void InsNewSkill()
        {
            //  Общая идея - удалить из списка те, кторые уже выбраны. Потом передать их в форму.
            var ls = ((BindingList<SkillsViewModel>)dataGridViewSkills.DataSource)?.Select(z => z.SkillId).ToList();

            if (ls == null)
                return;

            var list = Skills.Where(x => !ls.Contains(x.Id)).ToList();
                //Except(ls.Select(x => new Skill() { Id = x.SkillId, Name = x.SkillName }).ToList());

            var form = new FormSkills();
            form.StartShow(list);
            if (form.DialogResult == DialogResult.OK)
            {
                //  Записать в PersonSkill
                var skill = form.skill;
                var firstId = GetCurrentFirstId();
                var personSkill = new PersonSkill() { SkillId = skill.Id, PersonId = firstId };
                if (Service.AddPersonSkill(personSkill))
                {
                }
            }

            _skills = null;
            UpdateSkills();
        }

        private Guid GetCurrentFirstId()
        {
            if (dataGridView1.CurrentRow == null)
                return Guid.Empty;

            var viewModel = Data[dataGridView1.CurrentRow.Index];
            return viewModel.Id;
        }
        /// <summary>
        /// Сохранение отредактированных значений уровней навыков
        /// </summary>
        private void SaveCurrentSkills()
        {
            var ls = ((BindingList<SkillsViewModel>)dataGridViewSkills.DataSource)?.ToList();

            if (ls == null)
                return;

            var list = ls.Select(x => new PersonSkill()
              { SkillId = x.SkillId, PersonId = x.FirstId, Quantity = x.Quantity }).ToList();

            Service.AddPersonSkillList(list);
            UpdateSkills();
            //  Сохранить всё - имеющееся обновить, отсуствующее добавить
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SkillsEditModeChange(false);
            SaveCurrentSkills();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            SkillsEditModeChange(false);
            UpdateSkills();
        }

        private void buttonEditSkill_Click(object sender, EventArgs e)
        {
            if (dataGridViewSkills.RowCount > 0)
                SkillsEditModeChange(true);
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewSkills.CurrentRow == null)
                return;

            var index = dataGridViewSkills.CurrentRow.Index;
            var ps = ((BindingList<SkillsViewModel>)dataGridViewSkills.DataSource).ToList()[index];

            Service.DeletePersonSkill(ps.FirstId, ps.SkillId);
            UpdateSkills();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            EditPerson();
        }

        private void EditPerson()
        {
            if (dataGridView1.CurrentRow == null)
                return;

            var pers = Data[dataGridView1.CurrentRow.Index];
            var person = persons.FirstOrDefault(x => x.Id == pers.Id);
            if (person != null)
            {
                var form = new FormPersonAdd();
                form.StartShow(person);
                if (form.DialogResult == DialogResult.OK)
                {
                    if (Service.UpdatePerson(form.Person))
                        UpdatePersons();
                }
            }
        }

        private void dataGridViewSkills_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Ошибка формата данных. Исправьте значения.");
        }
    }
}
