﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeRecords.Domain.Models;
using EmployeeRecords.Domain.Services;

namespace EmployeeRecords.Forms
{
    public partial class FormStaffAdd : Form
    {
        private Staff Staff = new Staff();
        private Action SaveDataAction { get; set; }

        public FormStaffAdd()
        {
            InitializeComponent();
            SaveDataAction = SaveData;
        }
        public void StartShow(Staff staff)
        {
            //  Другое сохранение данных
            SaveDataAction = UpdateData;
            Staff = staff;
            button1.Text = "Обновить";
            textBoxName.Text = staff.Name;
            textBoxQ.Text = staff.Quantity.ToString();
            ShowDialog();
        }

        private void SaveData()
        {
            var service = new StaffService();
            service.Add(Staff);
            
        }
        private void UpdateData()
        {
            var service = new StaffService();
            service.Update(Staff);
        }


        private void Button1_Click(object sender, EventArgs e)
        {
            string name = textBoxName.Text.Trim();
            if (string.IsNullOrWhiteSpace(name))
                return;
            if (!float.TryParse(textBoxQ.Text, out float val))
                return;
            //  Need to Add val and name
            Staff.Name = name;
            Staff.Quantity = val;

            var service = new StaffService(); // StaffService.GetSkillService();
            SaveDataAction();
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
