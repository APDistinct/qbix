﻿using EmployeeRecords.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EmployeeRecords.Domain.Services;

namespace EmployeeRecords.Forms
{
    public partial class FormStaffPersonAdd : Form
    {
        private EmployeeService Service { get; set; } = new EmployeeService();

        public List<Person> SelectedPersons { get; private set; } = new List<Person>();
        private Guid? StaffId { get; set; }

        public FormStaffPersonAdd(Guid? staffId)
        {
            InitializeComponent();
            StaffId = staffId;

            UpdatePersons();
        }

        private void UpdatePersons()
        {
            var persons = Service.GetPersons();

            if (StaffId.HasValue)
            {
                var staffPersons = Service.GetPersonsByStaff(StaffId.Value);
                foreach (var staffPerson in staffPersons)
                {
                    var person = persons.FirstOrDefault(a => a.Id == staffPerson.Id);
                    if (person == null)
                        continue;

                    persons.Remove(person);
                }
            }

            checkedListBox1.DataSource = persons;
            checkedListBox1.DisplayMember = "Name";
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            var form = new FormPersonAdd();
            if (form.ShowDialog() == DialogResult.OK)
            {
                Service.AddPerson(form.Person);
            }

            UpdatePersons();
        }

        private void ButtonChoice_Click(object sender, EventArgs e)
        {
            SelectedPersons.Clear();

            foreach (Person selectedItem in checkedListBox1.SelectedItems)
            {
                SelectedPersons.Add(selectedItem);
            }

            DialogResult = DialogResult.OK;
            Close();
        }

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
            Close();
        }


        private void CheckedListBox1_ItemCheck_1(object sender, ItemCheckEventArgs e)
        {
            for (int ix = 0; ix < checkedListBox1.Items.Count; ++ix)
                if (ix != e.Index)
                    checkedListBox1.SetItemChecked(ix, false);
        }
    }
}
