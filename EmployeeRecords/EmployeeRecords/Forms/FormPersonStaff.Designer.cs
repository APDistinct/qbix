﻿namespace EmployeeRecords.Forms
{
    partial class FormPersonStaff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewSkills = new System.Windows.Forms.DataGridView();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonEditSkill = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonCancelSkill = new System.Windows.Forms.Button();
            this.buttonEditSkills = new System.Windows.Forms.Button();
            this.buttonAddSkill = new System.Windows.Forms.Button();
            this.buttonDeleteSkill = new System.Windows.Forms.Button();
            this.buttonSaveSkill = new System.Windows.Forms.Button();
            this.dataGridViewStaffSkills = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSkills)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStaffSkills)).BeginInit();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(810, 6);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(83, 30);
            this.listBox1.TabIndex = 1;
            this.listBox1.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(104, 23);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(217, 21);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Должность";
            // 
            // dataGridViewSkills
            // 
            this.dataGridViewSkills.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSkills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSkills.Location = new System.Drawing.Point(641, 77);
            this.dataGridViewSkills.Name = "dataGridViewSkills";
            this.dataGridViewSkills.Size = new System.Drawing.Size(263, 251);
            this.dataGridViewSkills.TabIndex = 3;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAdd.Location = new System.Drawing.Point(12, 367);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(182, 23);
            this.buttonAdd.TabIndex = 4;
            this.buttonAdd.Text = "Добавить(приём на должность)";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExit.Location = new System.Drawing.Point(821, 367);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(83, 23);
            this.buttonExit.TabIndex = 4;
            this.buttonExit.Text = "Выход";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.ButtonExit_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 77);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(370, 251);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            this.dataGridView1.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowEnter);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(119, 334);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 8;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonEditSkill
            // 
            this.buttonEditSkill.Location = new System.Drawing.Point(12, 334);
            this.buttonEditSkill.Name = "buttonEditSkill";
            this.buttonEditSkill.Size = new System.Drawing.Size(101, 23);
            this.buttonEditSkill.TabIndex = 6;
            this.buttonEditSkill.Text = "Редактировать";
            this.buttonEditSkill.UseVisualStyleBackColor = true;
            this.buttonEditSkill.Click += new System.EventHandler(this.buttonEditSkill_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(211, 334);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 7;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDelete.Location = new System.Drawing.Point(200, 367);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(192, 23);
            this.buttonDelete.TabIndex = 9;
            this.buttonDelete.Text = "Удалить(увольнение с должности)";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonCancelSkill
            // 
            this.buttonCancelSkill.Enabled = false;
            this.buttonCancelSkill.Location = new System.Drawing.Point(818, 334);
            this.buttonCancelSkill.Name = "buttonCancelSkill";
            this.buttonCancelSkill.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelSkill.TabIndex = 12;
            this.buttonCancelSkill.Text = "Отмена";
            this.buttonCancelSkill.UseVisualStyleBackColor = true;
            this.buttonCancelSkill.Visible = false;
            this.buttonCancelSkill.Click += new System.EventHandler(this.ButtonCancelSkill_Click);
            // 
            // buttonEditSkills
            // 
            this.buttonEditSkills.Enabled = false;
            this.buttonEditSkills.Location = new System.Drawing.Point(630, 335);
            this.buttonEditSkills.Name = "buttonEditSkills";
            this.buttonEditSkills.Size = new System.Drawing.Size(101, 23);
            this.buttonEditSkills.TabIndex = 10;
            this.buttonEditSkills.Text = "Редактировать";
            this.buttonEditSkills.UseVisualStyleBackColor = true;
            this.buttonEditSkills.Visible = false;
            this.buttonEditSkills.Click += new System.EventHandler(this.Button2_Click);
            // 
            // buttonAddSkill
            // 
            this.buttonAddSkill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddSkill.Location = new System.Drawing.Point(634, 367);
            this.buttonAddSkill.Name = "buttonAddSkill";
            this.buttonAddSkill.Size = new System.Drawing.Size(75, 23);
            this.buttonAddSkill.TabIndex = 13;
            this.buttonAddSkill.Text = "Добавить";
            this.buttonAddSkill.UseVisualStyleBackColor = true;
            this.buttonAddSkill.Visible = false;
            this.buttonAddSkill.Click += new System.EventHandler(this.ButtonAddSkill_Click);
            // 
            // buttonDeleteSkill
            // 
            this.buttonDeleteSkill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDeleteSkill.Location = new System.Drawing.Point(715, 367);
            this.buttonDeleteSkill.Name = "buttonDeleteSkill";
            this.buttonDeleteSkill.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteSkill.TabIndex = 14;
            this.buttonDeleteSkill.Text = "Удалить";
            this.buttonDeleteSkill.UseVisualStyleBackColor = true;
            this.buttonDeleteSkill.Visible = false;
            // 
            // buttonSaveSkill
            // 
            this.buttonSaveSkill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveSkill.Enabled = false;
            this.buttonSaveSkill.Location = new System.Drawing.Point(748, 335);
            this.buttonSaveSkill.Name = "buttonSaveSkill";
            this.buttonSaveSkill.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveSkill.TabIndex = 11;
            this.buttonSaveSkill.Text = "Сохранить";
            this.buttonSaveSkill.UseVisualStyleBackColor = true;
            this.buttonSaveSkill.Visible = false;
            this.buttonSaveSkill.Click += new System.EventHandler(this.Button4_Click);
            // 
            // dataGridViewStaffSkills
            // 
            this.dataGridViewStaffSkills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStaffSkills.Location = new System.Drawing.Point(388, 77);
            this.dataGridViewStaffSkills.Name = "dataGridViewStaffSkills";
            this.dataGridViewStaffSkills.Size = new System.Drawing.Size(247, 251);
            this.dataGridViewStaffSkills.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(450, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Требуемый уровень навыков";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(686, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(167, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Фактический уровень навыков";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(347, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "   ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(595, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(450, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Проверка на превышение";
            // 
            // FormPersonStaff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 402);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewStaffSkills);
            this.Controls.Add(this.buttonCancelSkill);
            this.Controls.Add(this.buttonEditSkills);
            this.Controls.Add(this.buttonAddSkill);
            this.Controls.Add(this.buttonDeleteSkill);
            this.Controls.Add(this.buttonSaveSkill);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonEditSkill);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.dataGridViewSkills);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label2);
            this.Name = "FormPersonStaff";
            this.Text = "Просмотр, приём. увольнение работников";
            this.Load += new System.EventHandler(this.FormPersonSkills_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSkills)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStaffSkills)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridViewSkills;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonEditSkill;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonCancelSkill;
        private System.Windows.Forms.Button buttonEditSkills;
        private System.Windows.Forms.Button buttonAddSkill;
        private System.Windows.Forms.Button buttonDeleteSkill;
        private System.Windows.Forms.Button buttonSaveSkill;
        private System.Windows.Forms.DataGridView dataGridViewStaffSkills;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
    }
}