﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeRecords.Domain.Models;
using EmployeeRecords.Domain.Services;
using EmployeeRecords.Models;

namespace EmployeeRecords.Forms
{
    public partial class FormPersonStaff : Form
    {
        private EmployeeService Service { get; } = new EmployeeService();

        private List<Skill> _skills;
        private List<Skill> Skills => _skills ?? (_skills = Service.GetSkills());

        private BindingList<PersonQModel> Data { get; set; }
        private List<Person> persons { get; set; }
        private List<Skill> skills { get; set; }
        private List<PersonSkill> personSkill { get; set; }

        private bool SkillsEditMode { get; set; }

        public FormPersonStaff()
        {
            InitializeComponent();
            UpdateStaffs();
            UpdatePersons();
            InitGrid();
            SkillsEditModeChange(false);
            EnableTrigger();
        }

        private void FormPersonSkills_Load(object sender, EventArgs e)
        {
            PersonsEditModeChange(false);
            dataGridView1.Focus();
        }

        private void PersonsEditModeChange(bool val)
        {
            var staff = comboBox1.SelectedItem as Staff;
            if (staff == null)
            {
                return;
            }
            SkillsEditMode = val;
            buttonAdd.Enabled = !SkillsEditMode;
            buttonDelete.Enabled = !SkillsEditMode;

            buttonEditSkill.Enabled = !SkillsEditMode;
            buttonSave.Enabled = SkillsEditMode;
            buttonCancel.Enabled = SkillsEditMode;
            if (dataGridViewSkills.ColumnCount > 0)
                dataGridViewSkills.Columns[1].ReadOnly = !SkillsEditMode;
        }

        private void InitGrid()
        {
            var staff = comboBox1.SelectedItem as Staff;
            if (staff == null)
            {
                return;
            }

            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;

            if (dataGridView1.ColumnCount > 0)
            {
                dataGridView1.Columns[0].HeaderText = "ФИО";
                dataGridView1.Columns[0].Width = 200;
                dataGridView1.Columns[0].ReadOnly = true;
                dataGridView1.Columns[1].HeaderText = "Ставка";
                dataGridView1.Columns[1].ReadOnly = false;
            }

            if (dataGridView1.RowCount > 0)
                dataGridView1.CurrentCell = dataGridView1[0, 0];
            dataGridView1.Focus();
        }

        private void InitGridViewSkills()
        {
            if (dataGridViewSkills.ColumnCount > 0)
            {
                dataGridViewSkills.Columns[0].HeaderText = "Навыки";
                dataGridViewSkills.Columns[0].ReadOnly = true;
            }

            if (dataGridViewSkills.ColumnCount > 1)
            {
                dataGridViewSkills.Columns[1].HeaderText = "Уровень";
                dataGridViewSkills.Columns[1].ReadOnly = true;
            }
            //dataGridView1.Columns[0].Width = dataGridView1.Width - 50; //400;
        }

        private void InitGridViewStaffSkills()
        {

            if (dataGridViewStaffSkills.ColumnCount > 1)
            {
                dataGridViewStaffSkills.Columns[0].HeaderText = "Навыки";
                dataGridViewStaffSkills.Columns[0].ReadOnly = true;
            }

            if (dataGridViewSkills.ColumnCount > 1)
            {
                dataGridViewStaffSkills.Columns[1].HeaderText = "Уровень";
                dataGridViewStaffSkills.Columns[1].ReadOnly = true;
            }

            //dataGridView1.Columns[0].Width = dataGridView1.Width - 50; //400;
        }

        private BindingList<PersonQModel> GetPersonList()
        {
            if (comboBox1.SelectedItem == null)
                return new BindingList<PersonQModel>();

            var staffId = ((Staff)comboBox1.SelectedItem).Id;
            var personStaffs = Service.GetPersonStaffs(staffId);
            var list = new BindingList<PersonQModel>(persons.Select(a => new PersonQModel
            {
                Id = a.Id,
                Name = a.Name,
                Quantity = personStaffs.FirstOrDefault(b => b.PersonId == a.Id)?.Quantity ?? 0
            }).ToList());
            return list;
        }

        private void UpdatePersons()
        {
            var staff = comboBox1.SelectedItem as Staff;
            if (staff == null)
            {
                return;
            }

            persons = Service.GetPersonsByStaff(staff.Id);

            //persons = Service.GetPersons();
            Data = new BindingList<PersonQModel>(GetPersonList())
            { AllowNew = false, AllowEdit = true, AllowRemove = false };
            dataGridView1.DataSource = Data;

            if (dataGridView1.RowCount > 0)
                dataGridView1.CurrentCell = dataGridView1[0, 0];
            dataGridView1.Focus();
        }

        private void UpdateStaffs()
        {
            var staffs = Service.GetStaffs();
            var data = new BindingList<Staff>(staffs) { AllowNew = false, AllowEdit = false, AllowRemove = false };
            comboBox1.DataSource = data;
            comboBox1.DisplayMember = "Name;Quantity";
            comboBox1.ValueMember = "Name";

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var staff = comboBox1.SelectedItem as Staff;
            if (staff == null)
            {
                return;
            }

            label4.Text = $@"{staff.Quantity}";

            UpdatePersons();
            UpdateStaffSkills();
            UpdateSkills();
        }

        private IList<SkillsViewModel> GetSkillsViewModel(List<Skill> skills)
        {
            return skills.Select(a => new SkillsViewModel
            {
                SkillId = a.Id,
                SkillName = a.Name
            }).ToList();
        }

        private List<SkillsViewModel> GetSkillsViewModel(List<PersonSkill> skills, Guid firstId)
        {
            return skills.Select(a => new SkillsViewModel
            {
                FirstId = firstId,
                SkillId = a.SkillId,
                Quantity = a.Quantity,
                SkillName = Skills.First(b => b.Id == a.SkillId).Name
            }).ToList();
        }

        private void ShowSkills(DataGridViewCellEventArgs e)
        {
            var rowindex = e.RowIndex;
            if (rowindex < 0)
                return;
            UpdateSkills(rowindex);
        }       

        private void UpdateSkills()
        {
            if (dataGridView1.CurrentRow == null)
            {
                dataGridViewSkills.DataSource = null;
                dataGridViewStaffSkills.DataSource = null;
                return;
            }
            var index = dataGridView1.CurrentRow.Index;
            UpdateSkills(index);
        }

        private void UpdateSkills(int index)
        {
            if (Data == null)
                return;
            
            var person = Data[index];

                personSkill = Service.GetPersonSkills(person.Id);

                var data = new BindingList<SkillsViewModel>(GetSkillsViewModel(personSkill, person.Id))
                { AllowNew = false, /*AllowEdit = true,*/ AllowRemove = false };
                data.AllowEdit = true;
                dataGridViewSkills.DataSource = data;
                InitGridViewSkills();
        }

        private void UpdateStaffSkills()
        {
            if (comboBox1.SelectedItem == null)
                return;

            var staff = (Staff)comboBox1.SelectedItem;
            var staffSkill = Service.GetStaffSkills(staff.Id);
            
            var data = new BindingList<SkillsViewModel>(GetStaffSkillsViewModel(staffSkill, staff.Id))
            { AllowNew = false, AllowEdit = false, AllowRemove = false };            
            dataGridViewStaffSkills.DataSource = data;
            InitGridViewStaffSkills();
        }

        private List<SkillsViewModel> GetStaffSkillsViewModel(List<StaffSkill> skills, Guid firstId)
        {
            return skills.Select(a => new SkillsViewModel
            {
                FirstId = firstId,
                SkillId = a.SkillId,
                Quantity = a.Quantity,
                SkillName = Skills.First(b => b.Id == a.SkillId).Name
            }).ToList();
        }

        private void AddPersons()
        {
            var staff = comboBox1.SelectedItem as Staff;
            if (staff == null)
            {
                return;
            }

            var staffId = ((Staff)comboBox1.SelectedItem).Id;
            var form = new FormStaffPersonAdd(staffId);
            if (form.ShowDialog() == DialogResult.OK)
            {
                var staffPersons = form.SelectedPersons.Select(a => new PersonStaff()
                { PersonId = a.Id, StaffId = staffId }).ToList();

                Service.AddStaffPerson(staffPersons);
            }

            UpdatePersons();
            UpdateStaffSkills();
        }

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            AddPersons();        
        }
       
        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            ShowSkills(e);
        }

        private void buttonEditSkill_Click(object sender, EventArgs e)
        {
            PersonsEditModeChange(true);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            PersonsEditModeChange(false);
            SaveCurrentPersons();
            SaveCurrentSkills();
        }

        private void SaveCurrentPersons()
        {
            var ls = ((BindingList<PersonQModel>)dataGridView1.DataSource)?.ToList();

            if (ls == null)
                return;

            var staffId = ((Staff)comboBox1.SelectedItem).Id;
            var list = ls.Select(x => new PersonStaff()
                { PersonId = x.Id, StaffId = staffId, Quantity = x.Quantity }).ToList();
            //  Сохранить всё - имеющееся обновить, отсуствующее добавить

            if(!Service.AddPersonStaffList(list, out string errStr))
                MessageBox.Show("Ошибка " + errStr);

            UpdatePersons();
            SaveCurrentSkills();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            PersonsEditModeChange(false);
            UpdatePersons();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            var staff = comboBox1.SelectedItem as Staff;
            if (staff == null)
            {
                return;
            }

            if (dataGridView1.CurrentRow == null)
            {
                return;
            }

            var index = dataGridView1.CurrentRow.Index;
            var personId = ((BindingList<PersonQModel>) dataGridView1.DataSource)[index].Id;

            Service.DeletePersonStaff(personId, staff.Id);

            UpdateStaffSkills();
            UpdatePersons();
            UpdateSkills();
        }

        
        private void Button2_Click(object sender, EventArgs e)
        {
            SkillsEditModeChange(true);
        }

        private void SkillsEditModeChange(bool val)
        {
            var staff = comboBox1.SelectedItem as Staff;
            if (staff == null)
            {
                return;
            }

            SkillsEditMode = val;
            buttonAddSkill.Enabled = !SkillsEditMode;
            buttonEditSkill.Enabled = !SkillsEditMode;

            buttonEditSkill.Enabled = !SkillsEditMode;
            buttonSaveSkill.Enabled = SkillsEditMode;
            buttonCancelSkill.Enabled = SkillsEditMode;
            buttonDeleteSkill.Enabled = !SkillsEditMode;
            buttonAddSkill.Enabled = !SkillsEditMode;
            if (dataGridViewSkills.ColumnCount > 0)
                dataGridViewSkills.Columns[1].ReadOnly = !SkillsEditMode;
            //dataGridViewSkills. .AllowEdit = !SkillsEditMode;
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            SkillsEditModeChange(false);
            SaveCurrentSkills();
        }

        private void SaveCurrentSkills()
        {
            var ls = ((BindingList<SkillsViewModel>)dataGridViewSkills.DataSource)?.ToList();

            if (ls == null)
                return;

            var list = ls.Select(x => new PersonSkill()
                { SkillId = x.SkillId, PersonId = x.FirstId, Quantity = x.Quantity }).ToList();
            //  Сохранить всё - имеющееся обновить, отсуствующее добавить

            Service.AddPersonSkillList(list);
            UpdatePersons();
            UpdateSkills();
        }

        private void ButtonCancelSkill_Click(object sender, EventArgs e)
        {
            SkillsEditModeChange(false);
            UpdateSkills();
        }

        private void ButtonAddSkill_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Ошибка формата данных. Исправьте значения.");
        }

        private void Button1_Click(object sender, EventArgs e)
        {

            if (trig)
                DisableTrigger();
            else
                EnableTrigger();

        }

        private bool trig;

        private void EnableTrigger()
        {
            trig = true;
            button1.Text = "Вкл";
            try
            {
                Service.EnableTrigger();
            }
            catch
            {
                MessageBox.Show("Ошибка");
            }
        }

        private void DisableTrigger()
        {
            trig = false;
            button1.Text = "Выкл";
            try
            {
                Service.DisableTrigger();
            }
            catch
            {
                MessageBox.Show("Ошибка");
            }            
        }
    }
}
