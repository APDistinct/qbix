﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeRecords.Domain.Models;
using EmployeeRecords.Domain.Services;

namespace EmployeeRecords.Forms
{
    public partial class FormSkills : Form
    {
        public Skill skill { get; private set; }
        private List<Skill> skills = new List<Skill>();

        public FormSkills()
        {
            InitializeComponent();
            var service = new EmployeeService();
            skills = service.GetSkills();
            UpdateSkills();
            buttonChoice.Visible = false;
        }
        public void StartShow(IEnumerable<Skill> list = null)
        {
            if(list != null)
            {
                skills = list.ToList(); //skills.Except(list).ToList();
                UpdateSkills();
            }
            buttonChoice.Visible = true;
            ShowDialog();
        }

        private void ButtonShowSkills_Click(object sender, EventArgs e)
        {
            UpdateSkills();
        }

        private void UpdateSkills()
        {   
            listBox1.DataSource = new BindingList<Skill>(skills);
            listBox1.DisplayMember = "Name";
            if(skills.Count > 0)
            {
                listBox1.SelectedIndex = 0;
            }

            //labelSkills.Text = string.Join("\n", skills.Select(a => a.Name));
        }

        private void ButtonAddSkill_Click(object sender, EventArgs e)
        {
            var form = new FormSkillAdd();
            if (form.ShowDialog() == DialogResult.OK)
            {
                var skill = new Skill {Name = form.SkillName};
                skills.Add(skill);
                var service = SkillService.GetSkillService();
                service.Add(skill);
            }
            UpdateSkills();
            if (listBox1.Items.Count > 0)
                listBox1.SelectedIndex = listBox1.Items.Count - 1;
            listBox1.Focus();

        }

        private void buttonChoice_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                return;

            var s = (BindingList<Skill>) listBox1.DataSource; 
            if (s == null)
                return;

            skill = s[listBox1.SelectedIndex];
            DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
            Close();
        }
    }
}
