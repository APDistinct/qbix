﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeRecords.Domain.Models;
using EmployeeRecords.Domain.Services;

namespace EmployeeRecords.Forms
{
    public partial class FormSkillAdd : Form
    {
        public string SkillName { get; private set; }
        public FormSkillAdd()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(textBox1.Text))
                return;
            SkillName = textBox1.Text.Trim();
            
            DialogResult = DialogResult.OK;
            Close();
        }

        private void FormSkillAdd_Load(object sender, EventArgs e)
        {

        }
    }
}
