﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeRecords.Domain.Models;


namespace EmployeeRecords.Forms
{
    public partial class FormPersonAdd : Form
    {
        public Person Person {get; private set; }= new Person();
        private byte[] bytesFoto = null;

        public FormPersonAdd()
        {
            InitializeComponent();
        }

        public FormPersonAdd(Person person) : base()
        {
            Person = person;            
        }

        public void StartShow(Person person)
        {
            Person = person;
            textBoxI.Text = Person.NameI;
            textBoxO.Text = Person.NameO;
            textBoxF.Text = Person.NameF;
            if (Person.Birthday != null)
                dateTimePickerB.Value = Person.Birthday.Value;
            if (person.Foto != null)
            {
                bytesFoto = person.Foto;
                ShowFoto();
            }
            ShowDialog();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Person.NameI = textBoxI.Text;
            Person.NameO = textBoxO.Text;
            Person.NameF = textBoxF.Text;
            Person.Birthday = dateTimePickerB.Value;
            Person.Foto = bytesFoto;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string fname = openFileDialog1.FileName;
                    bytesFoto = File.ReadAllBytes(fname);
                    if (bytesFoto.GetFileImageType() != null)
                    {
                        ShowFoto();                        
                    }
                    else
                    {
                        MessageBox.Show($"Неверный файл изображения {fname}");
                    }
                }
                catch(Exception ex)
                {

                }
            }    
        }
        private void ShowFoto()
        {
            MemoryStream ms = new MemoryStream(bytesFoto);
            Image img = Image.FromStream(ms);
            pictureBoxFoto.Image = img;
            ms.Close();
        }
    }
}
