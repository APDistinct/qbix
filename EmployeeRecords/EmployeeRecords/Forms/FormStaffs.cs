﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeRecords.Domain.Models;
using EmployeeRecords.Domain.Services;
using EmployeeRecords.Models;

namespace EmployeeRecords.Forms
{
    public partial class FormStaffs : Form
    {
        //private StaffService StaffService { get; } = new StaffService();
        //private SkillService SkillService { get; } = SkillService.GetService();
        private EmployeeService Service { get; set; } = new EmployeeService();

        private List<Skill> _skills;
        private List<Skill> Skills => _skills ?? (_skills = Service.GetSkills());

        private BindingList<StaffViewModel> Data { get; set; } = new BindingList<StaffViewModel>();

        public FormStaffs()
        {
            InitializeComponent();
            InitializeData();
        }

        private void InitializeData()
        {
            var ps = Service.GetPersonStaff();

            Data = new BindingList<StaffViewModel>(ps.Select(a => new StaffViewModel
            {
                Id = a.Id,
                Name = a.Name,
                QuantityShtats = a.Quantity ,
                QuantityFact = a.QuantityFact ,                
            }).ToList()) {AllowNew = false, AllowEdit = false, AllowRemove = false};
            dataGridView1.DataSource = Data;

            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.Columns[0].HeaderText = "Должность";
            dataGridView1.Columns[0].Width = 200;
            dataGridView1.Columns[1].HeaderText = "Кол-во по штату";
            dataGridView1.Columns[2].HeaderText = "Кол-во фактич";            

            if (dataGridView1.RowCount > 0)
                dataGridView1.CurrentCell = dataGridView1[0, 0];
            dataGridView1.Focus();
        }

        /// <summary>
        /// Добавление должности
        /// </summary>
        private void InsNewStuff()
        {
            // Ввод и фиксация нового
            // Добавление и установка курсора на него
            var newForm = new FormStaffAdd();
            if (newForm.ShowDialog() == DialogResult.OK)
            {
                InitializeData();
                //InitGrid();
                int r = dataGridView1.RowCount;
                if (dataGridView1.RowCount > 0)
                    dataGridView1.CurrentCell = dataGridView1[0, r - 1];
                dataGridView1.Focus();
            }
        }

        private void EditStuff()
        {
            if (dataGridView1.CurrentRow == null)
                return;

            var newForm = new FormStaffAdd();
            var staff = GetCurrentStaff();
            newForm.StartShow(staff);
            if (newForm.DialogResult == DialogResult.OK)
            {                
                //InitGrid();
                int r = dataGridView1.CurrentRow.Index;
                InitializeData();
                if (dataGridView1.RowCount > 0)
                    dataGridView1.CurrentCell = dataGridView1[0, r ];
                dataGridView1.Focus();
            }
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
           // ShowSkills(e);            

            //var staff = Data[e.RowIndex];

            //if (staff.Id == Guid.Empty)
            //{
            //    var data = new BindingList<StaffSkillsViewModel>(GetStaffSkillsViewModel(Skills))
            //    { AllowNew = true, AllowEdit = true, AllowRemove = true };
            //    dataGridViewSkills.DataSource = data;
            //}
            //else
            //{
            //    var skills = StaffService.GetSkills(staff.Id);

            //    var data = new BindingList<StaffSkillsViewModel>(GetStaffSkillsViewModel(skills, staff))
            //    { AllowNew = true, AllowEdit = true, AllowRemove = true }; 
            //    dataGridViewSkills.DataSource = data;
            //}
        }

        private IList<StaffSkillsViewModel> GetStaffSkillsViewModel(List<Skill> skills)
        {
            return skills.Select(a => new StaffSkillsViewModel
            {
                SkillId = a.Id,
                SkillName = a.Name
            }).ToList();
        }

        private List<SkillsViewModel> GetStaffSkillsViewModel(List<StaffSkill> skills, StaffViewModel staff)
        {
            return skills.Select(a => new SkillsViewModel
            {
                FirstId = staff.Id,
                SkillId = a.SkillId,
                Quantity = a.Quantity,
                SkillName = Skills.First(b => b.Id == a.SkillId).Name
            }).ToList();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Insert)
            {
                InsNewStuff();
            }
        }

        private void buttonAddStuff_Click(object sender, EventArgs e)
        {
            InsNewStuff();
        }

        private void buttonEditStuff_Click(object sender, EventArgs e)
        {
            EditStuff();
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {        
            ShowSkills(e);
        }

        private void ShowSkills(DataGridViewCellEventArgs e)
        {
            var rowindex = e.RowIndex;
            
            if (rowindex < 0)
                return;
            UpdateSkills(rowindex);            
        }

        private void dataGridViewSkills_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Insert)
            {
                InsNewSkill();
            }
        }

        private void buttonAddSkill_Click(object sender, EventArgs e)
        {
            InsNewSkill();
        }

        private void InsNewSkill()
        {
            //  Общая идея - удалить из списка те, кторые уже выбраны. Потом передать их в форму.
            var ls = ((BindingList<SkillsViewModel>)dataGridViewSkills.DataSource)?.Select(z => z.SkillId).ToList();

            if (ls == null)
                return;

            var list = Skills.Where(x => !ls.Contains(x.Id)).ToList();

            var form = new FormSkills();
            form.StartShow(list);
            if (form.DialogResult == DialogResult.OK)
            {
                //  Записать в StaffSkill
                var skill = form.skill;
                var staffId = GetCurrentStaffId();
                var staffSkill = new StaffSkill() { SkillId = skill.Id, StaffId = staffId };
                if (Service.AddStaffSkill(staffSkill))
                {
                    UpdateSkills();
                }
            }            
        }

        private void UpdateSkills()
        {
            if (dataGridView1.CurrentRow == null)
                return;
            var index = dataGridView1.CurrentRow.Index;
            UpdateSkills(index);
        }

        private void UpdateSkills(int index)
        {
            if (Data == null)
                return;

            var staff = Data[index];

            if (staff.Id != Guid.Empty)
            {
                var skills = Service.GetStaffSkills(staff.Id);

                var data = new BindingList<SkillsViewModel>(GetStaffSkillsViewModel(skills, staff))
                { AllowNew = false, AllowEdit = true, AllowRemove = true };
                data.AllowEdit = true;
                dataGridViewSkills.DataSource = data;
                InitGridViewSkills();
            }
        }

        private void InitGridViewSkills()
        {
            if (dataGridViewSkills.Columns.Count > 0)
            {
                dataGridViewSkills.Columns[0].HeaderText = "Навыки";
                dataGridViewSkills.Columns[0].ReadOnly = true;
            }

            if (dataGridViewSkills.Columns.Count > 1)
            {
                dataGridViewSkills.Columns[1].HeaderText = "Уровень";
                dataGridViewSkills.Columns[1].ReadOnly = false;
            }
            //dataGridView1.Columns[0].Width = dataGridView1.Width - 50; //400;
        }

        private Guid GetCurrentStaffId()
        {
            if (dataGridView1.CurrentRow == null || Data == null)
                return Guid.Empty;

            var viewModel = Data[dataGridView1.CurrentRow.Index];
            return viewModel.Id;
        }

        private Staff GetCurrentStaff()
        {
            if (dataGridView1.CurrentRow == null || Data == null)
                return new Staff();

            var viewModel = Data[dataGridView1.CurrentRow.Index];
            return new Staff() { Id = viewModel.Id, Name = viewModel.Name, Quantity = viewModel.QuantityShtats, };
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveStaffSkills();
        }

        private void SaveStaffSkills()
        {
            var ls = ((BindingList<SkillsViewModel>) dataGridViewSkills.DataSource);
            if (ls == null)
                return;

            var staffId = GetCurrentStaffId();
            var list = ls.Select(x => new StaffSkill() { SkillId = x.SkillId, StaffId = staffId, Quantity = x.Quantity }).ToList();
            Service.AddStaffSkillList(list);
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            DeleteStaffSkill();
        }

        private void DeleteStaffSkill()
        {
            var data = (BindingList<SkillsViewModel>)dataGridViewSkills.DataSource;

            if (dataGridViewSkills.CurrentRow == null || data == null)
                return;

            var deleteStaffSkill = data[dataGridViewSkills.CurrentRow.Index];
            data.Remove(deleteStaffSkill);
            Service.RemoveStaffSkill(deleteStaffSkill.FirstId, deleteStaffSkill.SkillId);
        }

        private void DataGridViewSkills_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            
        }

        private void dataGridViewSkills_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Ошибка формата данных. Исправьте значения.");
        }
    }
}
