﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeRecords.Domain.Services;
using EmployeeRecords.Forms;

namespace EmployeeRecords
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ButtonSkills_Click(object sender, EventArgs e)
        {
            var form = new FormSkills();
            form.Show();
        }

        private void buttonPersonSkills_Click(object sender, EventArgs e)
        {
            var form = new FormPersonStaff();
            form.Show();
        }

        private void buttonPersonAdd_Click(object sender, EventArgs e)
        {
            var form = new FormPersonAdd();
            if (form.ShowDialog() == DialogResult.OK)
            {
                var service = new EmployeeService();
                service.AddPerson(form.Person);
            }
        }

        private void buttonStaffs_Click(object sender, EventArgs e)
        {
            var form = new FormStaffs();
            form.Show();
        }

        private void ButtonPersons_Click(object sender, EventArgs e)
        {
            var form = new FormPersons();
            form.Show();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
