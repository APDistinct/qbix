﻿namespace EmployeeRecords
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSkills = new System.Windows.Forms.Button();
            this.buttonPersonSkills = new System.Windows.Forms.Button();
            this.buttonPersonAdd = new System.Windows.Forms.Button();
            this.buttonStaffs = new System.Windows.Forms.Button();
            this.buttonPersons = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonSkills
            // 
            this.buttonSkills.Location = new System.Drawing.Point(68, 74);
            this.buttonSkills.Name = "buttonSkills";
            this.buttonSkills.Size = new System.Drawing.Size(126, 34);
            this.buttonSkills.TabIndex = 1;
            this.buttonSkills.Text = "Навыки";
            this.buttonSkills.UseVisualStyleBackColor = true;
            this.buttonSkills.Click += new System.EventHandler(this.ButtonSkills_Click);
            // 
            // buttonPersonSkills
            // 
            this.buttonPersonSkills.Location = new System.Drawing.Point(68, 114);
            this.buttonPersonSkills.Name = "buttonPersonSkills";
            this.buttonPersonSkills.Size = new System.Drawing.Size(126, 34);
            this.buttonPersonSkills.TabIndex = 1;
            this.buttonPersonSkills.Text = "Форма работников";
            this.buttonPersonSkills.UseVisualStyleBackColor = true;
            this.buttonPersonSkills.Click += new System.EventHandler(this.buttonPersonSkills_Click);
            // 
            // buttonPersonAdd
            // 
            this.buttonPersonAdd.Location = new System.Drawing.Point(68, 154);
            this.buttonPersonAdd.Name = "buttonPersonAdd";
            this.buttonPersonAdd.Size = new System.Drawing.Size(126, 34);
            this.buttonPersonAdd.TabIndex = 1;
            this.buttonPersonAdd.Text = "Добавить работника";
            this.buttonPersonAdd.UseVisualStyleBackColor = true;
            this.buttonPersonAdd.Click += new System.EventHandler(this.buttonPersonAdd_Click);
            // 
            // buttonStaffs
            // 
            this.buttonStaffs.Location = new System.Drawing.Point(68, 34);
            this.buttonStaffs.Name = "buttonStaffs";
            this.buttonStaffs.Size = new System.Drawing.Size(126, 34);
            this.buttonStaffs.TabIndex = 1;
            this.buttonStaffs.Text = "Должности";
            this.buttonStaffs.UseVisualStyleBackColor = true;
            this.buttonStaffs.Click += new System.EventHandler(this.buttonStaffs_Click);
            // 
            // buttonPersons
            // 
            this.buttonPersons.Location = new System.Drawing.Point(68, 194);
            this.buttonPersons.Name = "buttonPersons";
            this.buttonPersons.Size = new System.Drawing.Size(126, 34);
            this.buttonPersons.TabIndex = 1;
            this.buttonPersons.Text = "Список работников";
            this.buttonPersons.UseVisualStyleBackColor = true;
            this.buttonPersons.Click += new System.EventHandler(this.ButtonPersons_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(68, 244);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(126, 31);
            this.buttonExit.TabIndex = 2;
            this.buttonExit.Text = "Выход";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 450);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonPersons);
            this.Controls.Add(this.buttonPersonAdd);
            this.Controls.Add(this.buttonPersonSkills);
            this.Controls.Add(this.buttonStaffs);
            this.Controls.Add(this.buttonSkills);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonSkills;
        private System.Windows.Forms.Button buttonPersonSkills;
        private System.Windows.Forms.Button buttonPersonAdd;
        private System.Windows.Forms.Button buttonStaffs;
        private System.Windows.Forms.Button buttonPersons;
        private System.Windows.Forms.Button buttonExit;
    }
}

