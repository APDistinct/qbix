﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace EmployeeRecords.Domain.Tests
{
    public abstract class AbstractTestClass : IDisposable
    {
        protected ITestLogger Logger { get; }
        protected MemoryService MemoryService { get; }
        private Stopwatch Stopwatch { get; }
        private long Memory { get; }

        protected bool UseBenchmark { get; set; }

        protected AbstractTestClass(ITestOutputHelper testOutputHelper)
        {
            Memory = GC.GetTotalMemory(true);
            MemoryService = new MemoryService();

            Logger = new TestLogger(testOutputHelper);
            Logger.Log($"Start at {DateTime.Now.ToLongTimeString()}");
            Logger.Log("");

            Stopwatch = Stopwatch.StartNew();
        }

        //[Fact]
        //public void Benchmark()
        //{
        //    if (!UseBenchmark)
        //    {
        //        Logger.Log($"Benchmark is turn off", Stopwatch.Elapsed.TotalMilliseconds);
        //        return;
        //    }

        //    const int repeatCont = 20;

        //    var methods = GetType().UnderlyingSystemType.GetMethods()
        //        .Where(a => a.GetCustomAttribute(typeof(FactAttribute)) != null && a.Name != nameof(Benchmark)).ToList();

        //    Logger.Log($"{methods.Count} methods invoked {repeatCont} times each");
        //    Logger.Log("");

        //    foreach (var method in methods)
        //    {
        //        try
        //        {
        //            Logger.TurnOff();
        //            method.Invoke(this, null);

        //            var sw = Stopwatch.StartNew();
        //            for (int i = 0; i < repeatCont; i++)
        //            {
        //                method.Invoke(this, null);
        //            }

        //            sw.Stop();

        //            Logger.TurnOn();
        //            Logger.Log($"{method.Name}: ", $"{(sw.Elapsed.TotalMilliseconds / repeatCont):N6}");
        //        }
        //        catch(Exception e)
        //        {
        //            Logger.TurnOn();
        //            Logger.Log($"{method.Name}: ", $"Has exception ({e.GetType().Name})");
        //        }
        //    }
        //}

        public void Dispose()
        {
            MemoryService.Stop();
            var infoMax = MemoryService.TimerEvents.OrderByDescending(a => a.MemoryInUse).FirstOrDefault() ?? new TimerEventInfo(DateTime.Now, GC.GetTotalMemory(true));
            var infoMin = MemoryService.TimerEvents.OrderBy(a => a.MemoryInUse).FirstOrDefault() ?? new TimerEventInfo(DateTime.Now, GC.GetTotalMemory(true));
            var methodUsed = infoMax.MemoryInUse - infoMin.MemoryInUse;

            Logger.Log("");
            Logger.Log("------------------------------------------------------------------------------------");
            Logger.Log("Method used:", Helper.ReadableFileSize(methodUsed));
            Logger.Log("Test used:", infoMax.Memory);
            //Logger.Log($"Time taken (method):", $"{(MemoryService.TimerTicks - 2) * 15}-{(MemoryService.TimerTicks - 1) * 15}");
            Logger.Log($"Timer Ticks:", MemoryService.TimerTicks);
            Logger.Log($"Time taken:", Stopwatch.Elapsed.TotalMilliseconds);
        }

    }

    public class MemoryService
    {
        public List<TimerEventInfo> TimerEvents { get; } = new List<TimerEventInfo>();
        public int TimerTicks { get; set; }

        private Timer timer;

        public MemoryService()
        {
            SetTimer();
        }

        private void SetTimer()
        {
            timer = new Timer(TimerTik, null, 0, 15);
        }

        public void TimerTik(object obj)
        {
            TimerEvents.Add(new TimerEventInfo(DateTime.Now, GC.GetTotalMemory(true)));
            TimerTicks++;
        }

        public void Stop()
        {
            timer.Dispose();
        }

        public void Restart()
        {
            TimerEvents.Clear();
            SetTimer();
        }
    }

    public class TimerEventInfo
    {
        public TimerEventInfo(DateTime signalTime, long memoryInUse)
        {
            SignalTime = signalTime;
            MemoryInUse = memoryInUse;
        }

        public DateTime SignalTime { get; set; }
        public long MemoryInUse { get; set; }

        public string Memory => Helper.ReadableFileSize(MemoryInUse);
    }

    public static class Helper
    {
        public static string ReadableFileSize(double size, int unit = 0)
        {
            string[] units = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

            while (size >= 1024)
            {
                size /= 1024;
                ++unit;
            }

            return $"{size:0.#} {units[unit]}";
        }
    }
}
