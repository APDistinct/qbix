﻿using System;
using System.Data.Linq;
using System.Linq;
using EmployeeRecords.Domain.Models;
using EmployeeRecords.Domain.Repository;
using EmployeeRecords.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using Xunit.Abstractions;
using Assert = Xunit.Assert;

namespace EmployeeRecords.Domain.Tests
{
    
    public class UnitTest1 : AbstractTestClass
    {
        private string ConnectionString =
            "Data Source=APD;Initial Catalog=QBIX;User ID=sa;Password=123;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public UnitTest1(ITestOutputHelper testOutputHelper) : base(testOutputHelper)
        {
        }

        [Fact]
        public void ReadFromDb()
        {
            var context = new DataContext(ConnectionString);
            var skills = context.GetTable<Skill>().ToList();

            Assert.NotNull(skills);
        }

        [Fact]
        public void Add_Delete()
        {
            var context = new DataContext(ConnectionString);
            var skill = new Skill {Name = "C##"};
            context.GetTable<Skill>().InsertOnSubmit(skill);
            context.SubmitChanges();

            var skills = context.GetTable<Skill>().ToList();

            var skill1 = skills.FirstOrDefault(a => a.Name == skill.Name);
            Assert.NotNull(skill1);
            Logger.Log(skill1);

            context.GetTable<Skill>().DeleteOnSubmit(skill);
            context.SubmitChanges();

            skills = context.GetTable<Skill>().ToList();

            skill1 = skills.FirstOrDefault(a => a.Name == skill.Name);
            Assert.Null(skill1);
        }

        [Fact]
        public void Edit()
        {

            using (var context = GetContext)
            {
                var skill = context.GetTable<PersonSkill>().FirstOrDefault();
                if (skill == null)
                {
                    Logger.Log("skills are empty");
                    return;
                }

                skill.Quantity = 10;
                context.SubmitChanges();
            }

            var skill1 = GetContext.GetTable<PersonSkill>().FirstOrDefault();
            Logger.Log(skill1);
        }


        [Fact]
        public void Select()
        {

            using (var context = GetContext)
            {
                var ps = context.GetTable<PersonStaff>()
                    .GroupBy(x => x.StaffId)
                    .Select(a => new StaffViewModel()
                    { Id = a.Key,
                        QuantityShtats = a.Sum(z => z.Quantity),
                        QuantityFact = a.Select(x => x.Quantity).First()/*, Name */ });
                //    FirstOrDefault();
                //skill.QuantitySh = 10;

                Logger.Log("ps count",ps.Count());

                foreach (var staffViewModel in ps)
                {
                    Logger.Log(staffViewModel);
                }
            }

            var pss = GetContext.GetPersonStaff();
            Logger.Log(pss.ReturnValue);
        }

        private QQbixContext GetContext => new QQbixContext(ConnectionString);
    }
}
