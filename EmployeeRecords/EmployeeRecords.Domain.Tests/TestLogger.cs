﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace EmployeeRecords.Domain.Tests
{
    public interface ITestLogger
    {
        void TurnOn();
        void TurnOff();
        void Log(string message);
        void Log(string name, object value);
        void Log<T>(T value) where T : class;
        void Log(Action func);
    }

    public class TestLogger : ITestLogger
    {
        private bool state = true;
        private ITestOutputHelper TestOutputHelper { get; }
        public TestLogger(ITestOutputHelper testOutputHelper)
        {
            TestOutputHelper = testOutputHelper;
        }

        public void TurnOn()
        {
            state = true;
        }

        public void TurnOff()
        {
            state = false;
        }

        public void Log(string message)
        {
            if (!state) return;

            TestOutputHelper.WriteLine(message);
        }

        public void Log(string name, object value)
        {
            if (!state) return;

            const int length = 32;
            if (name.Length > length)
            {
                name = name.Substring(0, length - 4);
            }
            var t = (int)Math.Ceiling((length - name.Length) / 4.0);
            var tabs = string.Join("", Enumerable.Repeat("\t", t));
            var message = $"{name}{tabs}{value}";
            TestOutputHelper.WriteLine(message);
        }

        public void Log<T>(T obj) where T : class
        {
            if (!state) return;

            Log($"\tClass Name\t{typeof(T).Name}");
            foreach (var property in typeof(T).GetProperties())
            {
                var name = property.Name;
                var value = property.GetValue(obj);

                switch (value)
                {
                    case Dictionary<string, string> dict:
                        {
                            Log($"{name} (Dictionary<string, string>)");

                            foreach (var item in dict)
                            {
                                Log($"\t{item.Key}", item.Value ?? "null");
                            }
                            break;  
                        }

                    default:
                        Log(name, value ?? "null");
                        break;
                }
            }

            Log("");
        }

        public void Log(Action func)
        {
            Log($"Start {func.Method.Name} at {DateTime.Now.ToLongTimeString()}");
            Log("");

            var memory = GC.GetTotalMemory(true);
            var sw = Stopwatch.StartNew();

            func();

            Log("");
            Log("------------------------------------------------------------------------------------");
            Log("Memory used:", Helper.ReadableFileSize(GC.GetTotalMemory(true) - memory));
            Log($"Time taken:", sw.Elapsed.TotalMilliseconds);
        }
    }
}
