USE [QBIX]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[Id] [uniqueidentifier] NOT NULL PRIMARY KEY DEFAULT NEWSEQUENTIALID(),
	[Name] [nvarchar](50) NULL
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[Id] [uniqueidentifier] NOT NULL PRIMARY KEY DEFAULT NEWSEQUENTIALID(),
	[Name] [nvarchar](50) NOT NULL,
	[Quantity] [float] NOT NULL,
)
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Skill](
	[Id] [uniqueidentifier] NOT NULL PRIMARY KEY DEFAULT NEWSEQUENTIALID(),
	[Name] [nvarchar](50) NOT NULL,
	)
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonSkill](
	[PersonId] [uniqueidentifier] NOT NULL,
	[SkillId] [uniqueidentifier] NOT NULL,
	[Quantity] [float] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_PersonSkill] PRIMARY KEY CLUSTERED 
(
	[PersonId] ASC,
	[SkillId] ASC
),
  CONSTRAINT [FK_PersonSkill_Skill] FOREIGN KEY([SkillId])
REFERENCES [dbo].[Skill] ([Id]),
  CONSTRAINT [FK_PersonSkill_PersonId] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([Id])
)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StaffSkill](
	[StaffId] [uniqueidentifier] NOT NULL,
	[SkillId] [uniqueidentifier] NOT NULL,
	[Quantity] [float] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_StaffSkill] PRIMARY KEY CLUSTERED 
(
	[StaffId] ASC,
	[SkillId] ASC
),
  CONSTRAINT [FK_StaffSkill_Skill] FOREIGN KEY([SkillId])
REFERENCES [dbo].[Skill] ([Id]),
  CONSTRAINT [FK_StaffSkill_Staff] FOREIGN KEY([StaffId])
REFERENCES [dbo].[Staff] ([Id])
)
GO

create procedure [GetPersonStaff]
as
begin

SELECT s.Name
	  ,s.Quantity
	  , s.[Id]
      , COALESCE(sum(ps.[Quantity]),0) as [QuantityFact]
	  
  FROM [QBIX].[dbo].[Staff] s 
  left join [PersonStaff] ps on (ps.[StaffId] = s.[Id])
  group by s.[Id],s.Name,s.Quantity
end
go


CREATE TRIGGER [PersonStaff_CheckQLimits]
   ON  [PersonStaff]
   AFTER INSERT, UPDATE
AS 
BEGIN
  DECLARE ic CURSOR FORWARD_ONLY FAST_FORWARD
  FOR  
with [ins] as
(   select i.[staffId], COALESCE(sum(i.[Quantity]),0) as sum_i
    from inserted i
    group by i.[StaffId]
),

[pst] as
(   select ps.[staffId], 
      COALESCE(sum(ps.[Quantity]),0) as sum_ps
   from [PersonStaff] ps
    group by ps.[StaffId]
)
	select i.[staffId], ps.sum_ps, s.[Quantity] as sum_s
	from ins  i
	left join pst ps on (ps.StaffId = i.StaffId)
	left join [Staff] s on (s.Id = i.StaffId)
        
  open ic;
  declare @sum_s float;
  declare @sum_ps float;
  declare @staffId uniqueidentifier;

  fetch next from ic into @staffId, @sum_ps, @sum_s;
  while @@FETCH_STATUS = 0
  begin
    if  @sum_ps > @sum_s
    begin
	  close ic;
      deallocate ic;
      throw 51001, 'Превышение фактического над штатным', 1;
    end
 
    fetch next from ic into @staffId, @sum_ps, @sum_s;
  END
  close ic;
  deallocate ic;
END
go

ALTER TABLE [PersonStaff] ENABLE TRIGGER [PersonStaff_CheckQLimits]


USE [master]
GO
ALTER DATABASE [QBIX] SET  READ_WRITE 
GO
