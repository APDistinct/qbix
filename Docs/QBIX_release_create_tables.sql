USE [QBIX]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[Id] [uniqueidentifier] NOT NULL PRIMARY KEY DEFAULT NEWSEQUENTIALID(),
	[NameF] nvarchar(50) NULL,
	[NameI] nvarchar(50) NULL,
	[NameO] nvarchar(50) NULL,
	[Birthday] date NULL,
	[Foto] varbinary(max) NULL
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[Id] [uniqueidentifier] NOT NULL PRIMARY KEY DEFAULT NEWSEQUENTIALID(),
	[Name] [nvarchar](50) NOT NULL,
	[Quantity] [float] NOT NULL,
)
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Skill](
	[Id] [uniqueidentifier] NOT NULL PRIMARY KEY DEFAULT NEWSEQUENTIALID(),
	[Name] [nvarchar](50) NOT NULL,
	)
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonSkill](
	[PersonId] [uniqueidentifier] NOT NULL,
	[SkillId] [uniqueidentifier] NOT NULL,
	[Quantity] [float] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_PersonSkill] PRIMARY KEY CLUSTERED 
(
	[PersonId] ASC,
	[SkillId] ASC
),
  CONSTRAINT [FK_PersonSkill_Skill] FOREIGN KEY([SkillId])
REFERENCES [dbo].[Skill] ([Id]),
  CONSTRAINT [FK_PersonSkill_PersonId] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([Id])
)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StaffSkill](
	[StaffId] [uniqueidentifier] NOT NULL,
	[SkillId] [uniqueidentifier] NOT NULL,
	[Quantity] [float] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_StaffSkill] PRIMARY KEY CLUSTERED 
(
	[StaffId] ASC,
	[SkillId] ASC
),
  CONSTRAINT [FK_StaffSkill_Skill] FOREIGN KEY([SkillId])
REFERENCES [dbo].[Skill] ([Id]),
  CONSTRAINT [FK_StaffSkill_Staff] FOREIGN KEY([StaffId])
REFERENCES [dbo].[Staff] ([Id])
)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonStaff](
	[PersonId] [uniqueidentifier] NOT NULL,
	[StaffId] [uniqueidentifier] NOT NULL,
	[Quantity] [float] NOT NULL,
 CONSTRAINT [PK_PersonStaff] PRIMARY KEY CLUSTERED 
(
	[PersonId] ASC,
	[StaffId] ASC
),
  CONSTRAINT [FK_PersonStaff_Staff] FOREIGN KEY([StaffId])
REFERENCES [dbo].[Staff] ([Id]),
  CONSTRAINT [FK_PersonStaff_Person] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([Id])
)
GO

USE [master]
GO
ALTER DATABASE [QBIX] SET  READ_WRITE 
GO
