use [QBIX]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'PersonStaff_CheckQLimits', N'TR') IS NOT NULL  
    DROP TRIGGER [PersonStaff_CheckQLimits];  
GO

CREATE TRIGGER [PersonStaff_CheckQLimits]
   ON  [PersonStaff]
   AFTER INSERT, UPDATE
AS 
BEGIN
  DECLARE ic CURSOR FORWARD_ONLY FAST_FORWARD
  FOR  
    select i.[staffId], sum(i.[Quantity]) as sum_i, sum(ps.[Quantity]) as sum_ps, sum(s.[Quantity]) as sum_s
	from inserted i		
	left join [PersonStaff] ps on (ps.StaffId = i.StaffId)
	left join [Staff] s on (s.Id = i.StaffId)
    group by i.[StaffId];
        
  open ic;
  declare @sum_i float;
  declare @sum_s float;
  declare @sum_ps float;
  declare @staffId uniqueidentifier;

  fetch next from ic into @staffId, @sum_i, @sum_ps, @sum_s;
  while @@FETCH_STATUS = 0
  begin
    if @sum_i+ @sum_ps > @sum_s
    begin
      throw 51001, 'Превышение фактического над штатным', 1;
    end
 
    fetch next from ic into @staffId, @sum_i, @sum_ps, @sum_s;
  END
  close ic;
  deallocate ic;
END

go
ALTER TABLE [PersonStaff] ENABLE TRIGGER [PersonStaff_CheckQLimits]
go