use [QBIX]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonStaff](
	[PersonId] [uniqueidentifier] NOT NULL,
	[StaffId] [uniqueidentifier] NOT NULL,
	[Quantity] [float] NOT NULL,
 CONSTRAINT [PK_PersonStaff] PRIMARY KEY CLUSTERED 
(
	[PersonId] ASC,
	[StaffId] ASC
),
  CONSTRAINT [FK_PersonStaff_Staff] FOREIGN KEY([StaffId])
REFERENCES [dbo].[Staff] ([Id]),
  CONSTRAINT [FK_PersonStaff_Person] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([Id])
)
GO
