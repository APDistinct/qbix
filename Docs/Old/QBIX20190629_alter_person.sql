use [QBIX]
go


alter table [Person]
drop column [Name]
go

alter table [Person]
add [NameF] nvarchar(50) NULL
go

alter table [Person]
add [NameI] nvarchar(50) NULL
go

alter table [Person]
add [NameO] nvarchar(50) NULL
go

alter table [Person]
add [Birthday] date NULL
go

alter table [Person]
add [Foto] varbinary(max) NULL
go