use [QBIX]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'PersonStaff_CheckQLimits', N'TR') IS NOT NULL  
    DROP TRIGGER [PersonStaff_CheckQLimits];  
GO

CREATE TRIGGER [PersonStaff_CheckQLimits]
   ON  [PersonStaff]
   AFTER INSERT, UPDATE
AS 
BEGIN
  DECLARE ic CURSOR FORWARD_ONLY FAST_FORWARD
  FOR  
with [ins] as
(   select i.[staffId], COALESCE(sum(i.[Quantity]),0) as sum_i
    from inserted i
    group by i.[StaffId]
),
--[del] as
--(   select d.[staffId], 
--      COALESCE(sum(d.[Quantity]),0) as sum_d
--    from deleted d
--    group by d.[StaffId]
--),

[pst] as
(   select ps.[staffId], 
      COALESCE(sum(ps.[Quantity]),0) as sum_ps
   from [PersonStaff] ps
    group by ps.[StaffId]
)
    --select i.[staffId], i.sum_i, d.sum_d, ps.sum_ps, s.[Quantity] as sum_s
	select i.[staffId], ps.sum_ps, s.[Quantity] as sum_s
	from ins  i
	left join pst ps on (ps.StaffId = i.StaffId)
	left join [Staff] s on (s.Id = i.StaffId)
	--left join del d on (d.StaffId = i.StaffId)

        
  open ic;
--  declare @sum_i float;
--  declare @sum_d float;
  declare @sum_s float;
  declare @sum_ps float;
  declare @staffId uniqueidentifier;

  --fetch next from ic into @staffId, @sum_i, @sum_d, @sum_ps, @sum_s;
  fetch next from ic into @staffId, @sum_ps, @sum_s;
  while @@FETCH_STATUS = 0
  begin
    --if @sum_i - @sum_d + @sum_ps > @sum_s
	if  @sum_ps > @sum_s
    begin
	  close ic;
      deallocate ic;
      throw 51001, 'Превышение фактического над штатным', 1;
    end
 
    fetch next from ic into @staffId, @sum_ps, @sum_s;
  --  fetch next from ic into @staffId, @sum_i, @sum_d, @sum_ps, @sum_s;
  END
  close ic;
  deallocate ic;
END

ALTER TABLE [PersonStaff] ENABLE TRIGGER [PersonStaff_CheckQLimits]
go