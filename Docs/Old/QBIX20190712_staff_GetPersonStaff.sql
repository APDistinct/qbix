use [QBIX]
go
IF OBJECT_ID (N'GetPersonStaff', N'P') IS NOT NULL  
    DROP procedure [GetPersonStaff]
GO  

create procedure [GetPersonStaff]
as
begin

SELECT s.Name
	  ,s.Quantity
	  , s.[Id]
      , COALESCE(sum(ps.[Quantity]),0) as [QuantityFact]
	  
  FROM [QBIX].[dbo].[Staff] s 
  left join [PersonStaff] ps on (ps.[StaffId] = s.[Id])
  group by s.[Id],s.Name,s.Quantity
end
go
