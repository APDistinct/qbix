use [QBIX]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'PersonStaff_CheckQLimits', N'TR') IS NOT NULL  
    DROP TRIGGER [PersonStaff_CheckQLimits];  
GO

CREATE TRIGGER [PersonStaff_CheckQLimits]
   ON  [PersonStaff]
   AFTER INSERT, UPDATE
AS 
BEGIN
  if exists(
    select i.[staffId]
	--, sum(i.[Quantity]) as sum_i, sum(ps.[Quantity]) as sum_ps, sum(s.[Quantity]) as sum_s
	from inserted i		
	left join [PersonStaff] ps on (ps.StaffId = i.StaffId)
	left join [Staff] s on (s.Id = i.StaffId)    
	group by i.[StaffId]
      having sum(i.[Quantity]) + sum(ps.[Quantity]) > sum(s.[Quantity]))
    begin
      throw 51001, 'Превышение фактического над штатным', 1;
    end
END

go
ALTER TABLE [PersonStaff] ENABLE TRIGGER [PersonStaff_CheckQLimits]
go